from flask import Flask, request, render_template, jsonify

from store import store
from store import cluster

app = Flask(__name__)


def get_mode(request):
    """Sets the context for execution.

    Note: This can be moved to middleware.

    """
    mode = request.args.get('mode')
    return cluster if mode == 'cluster' else store


@app.route('/')
@app.route('/help')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/get', methods=['GET', 'POST'])
def get_key():
    if request.method.lower() == 'post':
        key = request.get_json(force=True).get('key')

    elif request.method.lower() == 'get':
        key = request.args.get('key')

    client, message = get_mode(request).get_client()
    if not client:
        return jsonify(message)

    print('client', client)
    return jsonify(client.get(key))


@app.route('/set', methods=['POST'])
def set_key():
    set_payload = request.get_json(force=True)
    client, message = get_mode(request).get_client()
    if not client:
        return jsonify(message)

    return jsonify(client.set(**set_payload))


@app.route('/expire', methods=['POST', 'DELETE'])
def expire_key():
    if request.method.lower() == 'post':
        key = request.get_json(force=True).get('key')

    elif request.method.lower() == 'delete':
        key = request.args.get('key')

    client, message = get_mode(request).get_client()
    if not client:
        return jsonify(message)

    client.expire(key)
    return jsonify({})


@app.route('/admin', methods=['POST', 'GET', 'DELETE'])
def admin_handler():
    if request.method.lower() == 'post':
        cluster_config = request.get_json(force=True)
        _, message = get_mode(request).setup(**cluster_config)
        return jsonify(message)

    elif request.method.lower() == 'get':
        entity = get_mode(request)
        client, message = entity.get_client()
        if not client:
            return jsonify(message)
        return jsonify(entity.run_command('meta'))

    elif request.method.lower() == 'delete':
        message = get_mode(request).teardown()
        return jsonify(message)

    return jsonify({}), 400


@app.route('/admin/command', methods=['GET'])
def command_handler():
    command = request.args.get('command')
    return jsonify(get_mode(request).run_command(command))


@app.route('/demo/node/failure', methods=['GET'])
def demo_node_going_down():
    entity = get_mode(request)
    client, message = entity.get_client()
    if not client:
        return jsonify(message)
    client.remove_node()
    return jsonify(entity.run_command('inspect'))


@app.route('/demo/node/bringup', methods=['GET'])
def demo_node_bring_up():
    entity = get_mode(request)
    client, message = entity.get_client()
    if not client:
        return jsonify(message)

    client.add_node()
    return jsonify(entity.run_command('inspect'))
