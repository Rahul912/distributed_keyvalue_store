# Distributed KeyValue Store.
--------------------------

In memory fault-tolerant distributed database with configurable persitence.

## About:

There are 2 modes of operation.

1. __ReplicaSet mode__

- In ReplicaSt mode, database by-default boots with replication_factor = 2, i.e there will be 2 nodes where
  data will be replicated. Users can pass any replication_factor, while initializing the ReplicaSet.

- Both reads and write go to any random node in the ReplicaSet. Writes are propagated to rest of the nodes in a
  sychronous manner, however writes are not atomic, meaning theoretically the next read may not get the latest write.
  So, the system favours availability over consistency. Reads update the ttl of the keys in db.

- By default, all reads/writes are persisted on disk, this helps in recovering nodes after failure. With this even if a
  node goes down in between the operation and if comes up later, it will be updated with the latest writes.


2. __Cluster mode__

- In replica_set mode, if we add more node, we are just increasing HA, as data is replicated on all the nodes. However, in
  cluster mode, system boots up with n replica_sets (depending on setup). For distributing keys, a partitioning function
  is used. Keys will be replicated inside replica_set as per setup.

Note:
- *All the operation to database are agonstic to the mode selected i.e. in both mode, your client is capable of same
  oprations. All api contract remains same.*

- *If a opeation is meant to run on cluster, just pass `mode=cluster` in the API as qyery param. Check the exaple of
  setting up the database below.*

### Design Philosophy:

Ths database is based on [`Turtles all the way down`](https://en.wikipedia.org/wiki/Turtles_all_the_way_down) philosophy.
Each component is build on top of bottom components. In our case, `Node` are the smallest entity which stores the data, one or more `Nodes` makes a `ReplicaSet` and one or more replicaset makes a `cluster`.
Client can connect to a node/replicaset/cluster and the interface remains the same.


### Allowed Operations:

Operations can be broken in 3 parts.

- Basic functionality:
    1. __Get__: Gets the value and stdouts, along with value, it also stdouts, which node was being used to fetch the value.
    Users cant connect to a specfic node in replica_set, but system will choice a random node.
        ```
        curl -X GET http://0.0.0.0:5001/get?key=<key_name>
        ```

    2. __Set__: Sets the value for a key with an optional ttl. When ttl is not set, it can reside in memory for eternity.
        ```
        curl -X POST http://0.0.0.0:5001/set -d '{"key": "some_key", "value": "some_value", "ttl": 120}'
        ```

    3. __Expire__: Expires the key in database.
        ```
        curl -X DELETE http://0.0.0.0:5001/expire?key=<key_name>
        ```


- Admin functionality:
    1. __Create__: Setup the database with either in replica_set mode or in cluster mode. By default system boots in
       replica_set mode.

         ```
        curl -X POST http://0.0.0.0:5001/admin -d '{"replication_factor": 3}'
        curl -X POST http://0.0.0.0:5001/admin?mode=cluster -d '{"replication_factor": 3, "replica_sets": 2}'
        ```

    2. __Teardown__: Teardows the whole database. Removes the writes from the disk.
        ```
        curl -X DELETE http://0.0.0.0:5001/admin
        ```

- Command functionality:
    1. __Vaccum__: By default, memory are not reclaimed automatically even if key expires. Unless the key is read or key is explicitly expired, they stay in memory. Now, vaccum command can be used to recliam unused memory.

    2. __Inspect__: Stdouts the whole memory tree. This will be used in our case to verify if any oprations we are feeding
       is not working or not.
    3. __Meta__: Inspect with less verbosity. Only stdouts metadata.
    4. __Seed__: Fills the db with random seed data.

        ```
        curl -X GET http://0.0.0.0:5001/admin/command?command=<vaccum|inspect|meta|seed>
        ```

- Demo functionality:
For demo, system has exposed 2 demo functionality, one will add a node and one will remove the node. Both oprations
stdouts the current database state.

    ```
    curl -X GET http://0.0.0.0:5001/demo/node/failure
    curl -X GET http://0.0.0.0:5001/demo/node/bringup
    ```

### Setup:

1. Clone the code.
    ```
    git clone https://gitlab.com/Rahul912/distributed_keyvalue_store.git
    ```
2. Start project using docker.
    ```
    docker-compose up --build
    ```
3. (Optional) Now, db is ready to be used. Followig commands will create some seed data after setting up the db..
    ```
    curl -X POST http://0.0.0.0:5001/admin -d '{"replication_factor": 3}'
    curl -X GET http://0.0.0.0:5001/admin/command?command=seed
    curl -X GET http://0.0.0.0:5001/admin/command?command=inspect
    ```

### Future Scope:
1. Concept of `namespcae` could be introduced which will act as a db_table.
2. Currently writes on disk is `synchornous` which can be made async.
3. In cluster mode, we can cross place one of the nodes from replica set in different regions/AZs which can help us connect to db even in case of regional outage.
4. We can have snapshots which gets backed up in s3 or in-premise.

