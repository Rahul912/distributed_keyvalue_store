import hashlib
import random
from uuid import uuid4

from store import store

__all__ = ['get_client', 'setup', 'teardown', 'run_command']

cluster_client = None


def get_client():
    global cluster_client
    if not cluster_client:
        return None, {'message': 'Cluster is not created yet. Check /help', 'success': False}
    return cluster_client, {'message': 'Cluster created successfully', 'success': True}


def setup(replica_sets=1, replication_factor=2):
    global cluster_client
    if cluster_client:
        return cluster_client, {'message': 'Cluster already exists', 'success': False}
    cluster_client = Cluster(replica_sets=replica_sets, replication_factor=replication_factor)
    return cluster_client, {'message': 'Cluster created successfully', 'success': True}


def teardown():
    global cluster_client
    cluster_client, message = get_client()
    if cluster_client:
        cluster_client.tear_down()
        cluster_client = None
        return {'message': 'Teared down successfully', 'success': True}
    return {'message': 'Nothing to be teared-down', 'success': False}


def run_command(command):
    cluster_client, message = get_client()
    if not cluster_client:
        return message

    if not hasattr(cluster_client, command):
        return {'message': 'Incorrect command', 'success': False}

    return getattr(cluster_client, command)()


class Cluster(object):
    """Cluser is a collection of replication_set.

    Cluster is the gateway to access store. By-default, cluster will have 1 replica-set out of the box.

    Note: Once the cluster is setup, we can-not add more replica-sets. As keys are distributed using a
    partitioning function, reads might go to a different node in case of new nodes are being added in run-time.

    Solution like `Consistent Hashing` will not work, as it will just reduce the rehashing, however as a caching
    system, we should not result in cache-miss, when data sits on a different nodes.

    """

    def __init__(self, replica_sets=1, replication_factor=1):
        self._id = str(uuid4())
        self.replica_sets = [
            store.ReplicaSet(replica_set_id=i, replication_factor=replication_factor) for i in range(replica_sets)
        ]

    def set(self, key, value, ttl=None):
        replica_set = self._get_replica_set(key)
        replica_set.set(key=key, value=value, ttl=ttl)
        return {'_meta': {'replica_set': str(replica_set)}}

    def get(self, key):
        replica_set = self._get_replica_set(key)
        if replica_set.nodes:
            random_node = replica_set.nodes[random.randint(0, len(replica_set.nodes) - 1)]
            replica_set.current_node = random_node
        return {'_meta': {'replica_set': str(replica_set)}, 'value': replica_set.get(key)}

    def expire(self, key):
        replica_set = self._get_replica_set(key)
        replica_set.expire(key=key)

    def tear_down(self):
        for replica_set in self.replica_sets:
            replica_set.tear_down()
        del self.replica_sets

    def vaccum(self):
        for replica_set in self.replica_sets:
            replica_set.vaccum()
        return self.inspect()

    def inspect(self):
        return {'_id': self._id, 'replica_sets': [rs.inspect() for rs in self.replica_sets]}

    def meta(self):
        return {'_id': self._id, 'replica_sets': [rs.meta() for rs in self.replica_sets]}

    def _get_replica_set(self, key):
        replication_set_index = self._get_hash(key) % len(self.replica_sets)
        return self.replica_sets[replication_set_index]

    def _get_hash(self, key):
        return int(hashlib.sha256(key.encode('utf-8')).hexdigest(), 16) % 10 ** 8
