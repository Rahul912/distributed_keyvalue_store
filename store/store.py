import json
import os
import random
import time
from uuid import uuid4


__all__ = ['get_client', 'setup', 'teardown', 'run_command', 'ReplicaSet']

client = None


def get_client():
    global client
    if not client:
        return None, {'message': 'Nodes are not created yet.', 'success': False}
    if client.nodes:
        random_node = client.nodes[random.randint(0, len(client.nodes) - 1)]
        client.current_node = random_node
    return client, {'success': True}


def setup(replication_factor=3, *args, **kwargs):
    global client
    if client:
        return client, {'message': 'Nodes already exists', 'success': False}
    client = ReplicaSet(replication_factor=replication_factor)
    return client, {'message': 'Nodes created successfully', 'success': True}


def teardown():
    global client
    client, message = get_client()
    if client:
        client.tear_down()
        client = None
        return {'message': 'Teared down successfully', 'success': True}
    return {'message': 'Nothing to be teared-down', 'success': False}


def run_command(command):
    client, message = get_client()
    if not client:
        return message

    if not hasattr(client, command):
        return {'message': 'Incorrect command', 'success': False}

    return getattr(client, command)()


class ReplicaSet(object):
    def __init__(self, replica_set_id=0, replication_factor=1):
        assert replication_factor > 0, 'ReplicationFactor should be greate than 0.'
        self._id = str(uuid4())
        self.replica_set_id = replica_set_id
        self.nodes = [
            ReplicationNode(replica_set_id=self.replica_set_id, node_id=count) for count in range(replication_factor)
        ]
        self.current_node = None

    def __repr__(self):
        return 'replica_set-{}'.format(self.replica_set_id)

    def set(self, key, value, ttl=None):
        for node in self.nodes:
            node.set(key, value, ttl)
        return {'_meta': {'current_node': str(self.current_node)}}

    def get(self, key):
        return {'_meta': {'current_node': str(self.current_node)}, 'value': self.current_node.get(key)}

    def expire(self, key):
        for node in self.nodes:
            node.expire(key)

    def tear_down(self):
        for node in self.nodes:
            node.tear_down()
        del self.nodes

    def vaccum(self):
        for node in self.nodes:
            node.vaccum()
        return self.inspect()

    def add_node(self):
        last_node_id = self.nodes[-1].node_id if self.nodes else 1
        new_replica_node = ReplicationNode(restore=True, node_id=last_node_id + 1, replica_set_id=self.replica_set_id)
        self.nodes.append(new_replica_node)

    def remove_node(self):
        random_node_id = self._get_random_node_id()
        if random_node_id is None:
            return
        del self.nodes[random_node_id]

    def inspect(self):
        return self._get_info(verbose=True)

    def meta(self):
        return self._get_info(verbose=False)

    def seed(self):
        """Seeds db with random seed data."""
        for i in range(2):
            self.set(
                key=str(random.randint(10000000, 20000000)), value=random.randint(100, 200), ttl=random.randint(10, 300)
            )
        return self.inspect()

    def _get_info(self, verbose=False):
        return {
            '_meta': {'current_node': str(self.current_node), 'current_time': time.time()},
            'replica_set_id': self.replica_set_id,
            '_id': self._id,
            'nodes': [node.inspect(verbose=verbose) for node in self.nodes],
        }

    def _get_random_node_id(self):
        if self.nodes:
            random_node_ids_excluding_current_id = [
                i for i in range(len(self.nodes) - 1) if i != int(self.current_node.node_id)
            ]
            print('random_node_ids_excluding_current_id', random_node_ids_excluding_current_id)
            if random_node_ids_excluding_current_id:
                return random.choice(random_node_ids_excluding_current_id)
        return None


class ReplicationNode(object):
    def __init__(self, replica_set_id, node_id=0, restore=False):
        self._id = str(uuid4())
        self.replica_set_id = replica_set_id
        self.node_id = node_id
        self.store = {}
        self.backup_file = self._get_storage_backup()
        if restore:
            self._restore_from_disk()

    def __repr__(self):
        return '{}'.format(self.node_id)

    def inspect(self, verbose=False):
        node_info = {
            '_id': self._id,
            'node_id': self.node_id,
        }
        if verbose:
            node_info['store'] = self.store
        return node_info

    def set(self, key, value, ttl=None, expire_ts=None):
        val_to_be_stored = {
            'value': value,
            '_metadata': {
                'ttl': ttl,
                'expire_ts': expire_ts or (time.time() + ttl if ttl else None),
                'last_access_ts': time.time(),
                'key': key,
            },
        }
        self.store[key] = val_to_be_stored
        self._flush_to_disk(json.dumps(val_to_be_stored))

    def get(self, key):
        value_info = self.store.get(key)
        if not value_info:
            print('not value')
            return

        if self._has_expired(value_info):
            print('has_expired')
            self._handle_expired_value(key)
            return

        self._handle_non_expired_value(value_info)
        self._flush_to_disk(json.dumps(value_info))

        return value_info['value']

    def expire(self, key):
        """Idempotently expires the key.

        Notes: Expiry is not written on tbe disk, as it will be a costly operational.
        At the time of read from disk to memory, only non-expired key-value will be put back in memory.
        """
        value_info = self.store.get(key)
        if not value_info:
            return

        self._handle_expired_value(key)
        metadata = value_info['_metadata']
        metadata['expire_ts'] = time.time()
        self._flush_to_disk(json.dumps(value_info))

    def vaccum(self):
        """By default, expired keys are not re-claimed by its own. Either we need to read the value
        or run the vaccum command.

        """
        for key, value_info in self.store.items():
            if not self._has_expired(value_info):
                continue
            self._handle_expired_value(key)
        self.store = {key: value for key, value in self.store.items() if value}

    def tear_down(self):
        if os.path.isfile(self.backup_file):
            os.remove(self.backup_file)

    def _handle_expired_value(self, key):
        self.store[key] = {}

    @staticmethod
    def _handle_non_expired_value(value_info):
        metadata = value_info['_metadata']
        if metadata.get('expire_ts'):
            metadata['expire_ts'] += metadata.get('ttl', 0)
            metadata['last_access_ts'] = time.time()

    @staticmethod
    def _has_expired(value_info):
        metadata = value_info.get('_metadata', {})
        return metadata.get('expire_ts') and metadata.get('expire_ts') < time.time()

    def _flush_to_disk(self, value_info):
        persist_on_disk = os.getenv('STORE_PERSIST_ON_DISK', True)
        if not persist_on_disk:
            return

        with open(self.backup_file, 'a+') as file:
            print('value_info', value_info)
            file.write(value_info + '\n')

    def _get_storage_backup(self):
        backup_file = os.getenv('STORE_PERSISTENT_BACKUP')
        return '{}/replica_set_{}'.format(backup_file or '/tmp', self.replica_set_id)

    def _restore_from_disk(self):
        self.store = {}
        with open(self.backup_file, 'r') as file:
            for line in file.readlines():
                value_info = json.loads(line)
                if self._has_expired(value_info):
                    continue

                print('value_info', value_info)
                metadata = value_info['_metadata']
                metadata.pop('last_access_ts', None)
                self.store[metadata['key']] = {'value': value_info['value'], '_metadata': metadata}
